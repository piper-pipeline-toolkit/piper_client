# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from datetime import datetime
from piper_client.gui import Dialog
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import info_message, error_message, load_qt_ui
from piper_client.core.api import PiperClientApi
from piper_client.core.utils import environment_check, piper_except_hook


class LogWork(Dialog):
    """
    A widget used to log time spent working on a task/resource.
    """

    def __init__(self, parent):
        Dialog.__init__(self, parent, 'Piper Log Work', 'log_work.ui')
        self.api = PiperClientApi()
        self.task = self.api.get(entity_type='Task', entity_id=int(os.environ['PIPER_TASK_ID']))
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.headerLabel.setText('Logging work for: {0} -> {1} -> {2} -> {3}'.format(self.task.project.long_name,
                                                                                     self.task.link.sequence.long_name,
                                                                                     self.task.link.long_name,
                                                                                     self.task.long_name))

    def log_work(self):
        """
        Create a new work log for the current task.
        :return: error message if unsuccessful, otherwise - None;
        """
        description = self.descriptionTextEdit.toPlainText()
        if not description:
            return error_message(parent=self, msg='Please describe the work you are logging in the description field!')

        work_log = self.api.create(entity_type='WorkLog',
                                   name=str(datetime.now()),
                                   task=self.task,
                                   duration=self.durationSpinBox.value(),
                                   artist=self.api.get_current_artist(),
                                   description=description)

        info_message(parent=self,
                     msg='Logged {0} hours of work for {1} -> {2} -> {3} -> {4}!'.format(
                                                                                  work_log.duration,
                                                                                  self.task.project.long_name,
                                                                                  self.task.link.sequence.long_name,
                                                                                  self.task.link.long_name,
                                                                                  self.task.long_name))
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.logBtn.clicked.connect(self.log_work)


def show(parent=None):
    """
    Show the log dialog.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    if not parent:
        parent = QtWidgets.QApplication.activeWindow()
    if environment_check('PIPER_TASK_ID'):
        log_work = LogWork(parent=parent)
        log_work.exec_()
    else:
        msg = 'Piper Log Work: There is no task currently being worked on!'
        error_message(parent=parent, msg=msg)
        raise EnvironmentError(msg)
