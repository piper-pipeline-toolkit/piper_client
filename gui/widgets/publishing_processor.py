# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from piper_client.gui import Dialog
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.core.api import PiperClientApi


class PublishingProcessor(Dialog):
    """
    A dialog for displaying and running publishing processes.
    """

    def __init__(self, parent, process_type, processes):
        Dialog.__init__(self, parent, 'Piper Publishing Processor', 'publishing_processor.ui')
        self.api = PiperClientApi()
        self.process_type = process_type
        self.processes = dict()
        for i, process in enumerate(processes):
            self.processes[i] = {'process': process,
                                 'checkbox': None,
                                 'name_label': None,
                                 'fix_btn': None,
                                 'status': False}
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        if self.process_type == 'post_process':
            self.mainGrp.setTitle('Post-publishing processes')
            self.fixAllBtn.hide()

        for index in self.processes:
            process = self.processes[index]['process']
            layout = QtWidgets.QHBoxLayout(self)

            checkbox = QtWidgets.QCheckBox(self)
            if process.required:
                checkbox.setEnabled(False)
            checkbox.setChecked(self.processes[index]['process'].checked)
            self.processes[index]['checkbox'] = checkbox
            layout.addWidget(checkbox)

            name_label = QtWidgets.QLabel(process.name, self)
            name_label.setToolTip(process.description)
            self.processes[index]['name_label'] = name_label
            layout.addWidget(name_label)
            layout.addStretch()

            run_btn = QtWidgets.QPushButton('Run', self)
            run_btn.setToolTip(process.description)
            run_btn.clicked.connect(lambda i=index: self.run_process(index=i))
            layout.addWidget(run_btn)

            if self.process_type != 'post_process':
                fix_btn = QtWidgets.QPushButton('Fix', self)
                fix_btn.setEnabled(False)
                fix_btn.clicked.connect(lambda i=index: self.fix_process(index=i))
                self.processes[index]['fix_btn'] = fix_btn
                layout.addWidget(fix_btn)

            self.mainLayout.addLayout(layout)

    def run_process(self, index):
        """
        Run the process that corresponds to the given index.
        :param index: an (int) index;
        :return: None;
        """
        result = self.processes[index]['process'].run()
        self.processes[index]['status'] = result
        label_type = 'success'
        if not result:
            label_type = 'error'
            if self.processes[index]['fix_btn']:
                self.processes[index]['fix_btn'].setEnabled(True)
        self.processes[index]['name_label'].setProperty('label_type', label_type)
        self.evaluate()

    def fix_process(self, index):
        """
        Call the fix method of the process that corresponds to the given index.
        :param index: an (int) index;
        :return: None;
        """
        result = self.processes[index]['process'].fix()
        self.processes[index]['status'] = result
        if result:
            self.processes[index]['name_label'].setProperty('label_type', 'success')
        self.evaluate()

    def run_all(self):
        """
        Run all processes.
        :return: None;
        """
        for index in self.processes:
            if self.processes[index]['checkbox'].isChecked():
                self.run_process(index=index)

    def fix_all(self):
        """
        Call the fix method of all processes.
        :return: None;
        """
        for index in self.processes:
            if self.processes[index]['checkbox'].isChecked() and self.processes[index]['fix_btn'].isEnabled():
                self.fix_process(index=index)

    def evaluate(self):
        """
        Evaluate if all the processes have been successfully run and/or fixed
        and enable/disable the proceed button accordingly.
        :return: None;
        """
        status = True
        for index in self.processes:
            if self.processes[index]['checkbox'].isChecked() and not self.processes[index]['status']:
                status = False
                break
        self.proceedBtn.setEnabled(status)

    def proceed(self):
        """
        Set the parent Publisher widget's "proceed" attribute to True and close this dialog.
        :return: None;
        """
        self.parent.proceed = True
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.cancelBtn.clicked.connect(self.close)
        self.runAllBtn.clicked.connect(self.run_all)
        self.fixAllBtn.clicked.connect(self.fix_all)
        self.proceedBtn.clicked.connect(self.proceed)
