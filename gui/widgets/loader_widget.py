# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from piper_client.gui import Widget
from piper_client.gui.qt import QtGui, QtWidgets, QtCore
from piper_client.config import ICONS_PATH


class LoaderWidget(Widget):
    """
    An widget for displaying a loading message.
    """

    def __init__(self, parent=None):
        Widget.__init__(self, parent, 'Piper Loader', 'loader_widget.ui')
        self.gif = QtGui.QMovie(os.path.join(ICONS_PATH, 'loader.gif'), QtCore.QByteArray(), self)
        self.gif.setCacheMode(QtGui.QMovie.CacheAll)
        self.gif.setSpeed(100)
        self.gif_label.setMovie(self.gif)
        self.set_hidden(True)

    def set_hidden(self, value=True):
        self.setHidden(value)
        if value:
            self.gif.stop()

    def set_shown(self, value=True):
        if value:
            self.gif.start()
        self.setHidden(not value)
