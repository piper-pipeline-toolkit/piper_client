# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from piper_client.gui import Dialog
from piper_client.gui.qt import QtGui, QtWidgets
from piper_client.gui.utils import load_qt_ui, info_message, error_message
from piper_client.core.api import PiperClientApi
from piper_client.core.utils import environment_check, piper_except_hook


class CascadingConfig(Dialog):
    """
    An widget for viewing and editing configuration data for a given entity.
    """
    def __init__(self, entity_type, entity_id, mode='config', parent=None):
        Dialog.__init__(self, parent, 'Piper Cascading Config', 'cascading_config.ui')
        self.api = PiperClientApi()
        self.entity = self.api.get(entity_type=entity_type, entity_id=entity_id)
        self.config = self.api.get_config(entity=self.entity)
        self.inherited_config = self.api.get_inherited_config(entity=self.entity)
        self.entityLabel.setText('{0} {1}'.format(self.entity.type, self.entity.long_name))
        self._setup_data()
        if mode != 'config':
            self.closeBtn.clicked.connect(self.close)
            self.applyBtn.setHidden(True)
            self.addAttributeBtn.setHidden(True)
            self.deleteSelectedBtn.setHidden(True)
            self.ownConfigTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        else:
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        # Collect all inherited config data:
        if not self.inherited_config:
            return

        # Represent the collected parent config data in the respective table:
        self.inheritedConfigTable.setColumnCount(4)
        inherited_conifg_count = 0
        index = 0
        attr_list = []
        for i, config_data in sorted(self.inherited_config.items()):
            if int(i) > 1:
                inherited_conifg_count += len(config_data['data'])
                self.inheritedConfigTable.setRowCount(inherited_conifg_count)
                for attr, value in config_data['data'].items():
                    self.inheritedConfigTable.setItem(index, 0, QtWidgets.QTableWidgetItem(config_data['name']))
                    self.inheritedConfigTable.setItem(index, 1, QtWidgets.QTableWidgetItem(config_data['type']))
                    self.inheritedConfigTable.setItem(index, 2, QtWidgets.QTableWidgetItem(str(attr)))
                    self.inheritedConfigTable.setItem(index, 3, QtWidgets.QTableWidgetItem(str(value)))

                    self.inheritedConfigTable.item(index, 2).setForeground(QtGui.QColor(252, 252, 252))
                    self.inheritedConfigTable.item(index, 3).setForeground(QtGui.QColor(252, 252, 252))
                    if attr in attr_list:
                        color = QtGui.QColor(210, 49, 45)
                    else:
                        color = QtGui.QColor(49, 210, 45)
                        attr_list.append(attr)
                    self.inheritedConfigTable.item(index, 2).setBackground(color)
                    self.inheritedConfigTable.item(index, 3).setBackground(color)

                    index += 1

        self.inheritedConfigTable.resizeColumnsToContents()
        self.inheritedConfigTable.resizeRowsToContents()

        # Get own config data and represent it in the respective table:
        if len(self.inherited_config) > 1:
            own_config_data = self.inherited_config['1']
            if own_config_data:
                self.ownConfigTable.setColumnCount(2)
                self.ownConfigTable.setRowCount(len(own_config_data))
                index = 0
                for attr in sorted(own_config_data):
                    self.ownConfigTable.setItem(index, 0, QtWidgets.QTableWidgetItem(str(attr)))
                    self.ownConfigTable.setItem(index, 1, QtWidgets.QTableWidgetItem(str(own_config_data[attr])))
                    index += 1

        self.ownConfigTable.resizeColumnsToContents()
        self.inheritedConfigTable.resizeColumnsToContents()

    def add_attribute(self):
        """
        Add a new attribute/value row to the config table.
        :return: None;
        """
        table_row_count = self.ownConfigTable.rowCount()
        if table_row_count < 1:
            self.ownConfigTable.setColumnCount(2)
        self.ownConfigTable.setRowCount(table_row_count+1)

    def delete_attribute(self):
        """
        Delete the selected attribute row data.
        :return: None;
        """
        for item in self.ownConfigTable.selectedItems():
            item.setText('')

    def apply_config(self):
        """
        Create or update the entity's configuration with the attributes and values provided in its config table.
        :return: None;
        """
        new_config_data = {}
        for i in range(0, self.ownConfigTable.rowCount()):
            attribute = self.ownConfigTable.item(i, 0)
            value = self.ownConfigTable.item(i, 1)
            if attribute and value:
                value_data = value.text()
                # Check for floating point values:
                if value_data.replace('.', '').isdigit():
                    value_data = float(value.text())
                new_config_data[attribute.text()] = value_data

        # Create/update config:
        self.config = self.api.configure(entity=self.entity, config_rules=new_config_data, config=self.config)
        info_message(self, 'Configuration rules successfully applied!')
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.applyBtn.clicked.connect(self.apply_config)
        self.addAttributeBtn.clicked.connect(self.add_attribute)
        self.deleteSelectedBtn.clicked.connect(self.delete_attribute)


def show(parent=None):
    """
    Show the configuration dialog.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    if not parent:
        parent = QtWidgets.QApplication.activeWindow()
    if environment_check('PIPER_VERSION_ID'):
        entity_type = 'Version'
        entity_id = int(os.environ['PIPER_VERSION_ID'])
    elif environment_check('PIPER_RESOURCE_ID'):
        entity_type = 'Resource'
        entity_id = int(os.environ['PIPER_RESOURCE_ID'])
    elif environment_check('PIPER_TASK_ID'):
        entity_type='Task'
        entity_id=int(os.environ['PIPER_TASK_ID'])
    else:
        msg = 'Piper Cascading Config: no task, resource or version is currently being worked on.'
        error_message(parent=parent, msg=msg)
        raise EnvironmentError(msg)
    cascading_config = CascadingConfig(entity_type=entity_type,
                                       entity_id=entity_id,
                                       parent=parent,
                                       mode='display')
    cascading_config.exec_()
