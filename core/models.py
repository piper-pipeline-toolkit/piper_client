# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""

import os
import json
from dateutil.parser import parse
from piper_client.core.logger import get_logger
from piper_client.config import PIPER

LOGGER = get_logger('Piper Client Models')
BASE_FIELDS = ['type', 'id', 'name', 'long_name', 'description', 'created_by', 'created', 'updated_by', 'updated',
               'locked', 'metadata', 'display_fields', 'parent', 'children', 'dependency', 'history', 'full_url',
               'thumbnail_url', 'thumbnail_spec_url']


class Entity(object):
    """Piper Server entity representation class."""

    fields = []  # a list of fields applicable to this entity;
    fields_show = []  # a list of entity fields to exclusively display;
    _publish_path = None
    _work_path = None
    _thumbnail = None
    _thumbnail_spec = None
    _url = None
    path = None  # the localized disk path corresponding to the entity;

    def __init__(self, api, data):
        """
        Represent dictionary data as an entity object.
        :param api: a PiperClientApi instance;
        :param data: dictionary data;
        """
        super(Entity, self).__init__()
        self.api = api
        self.data = data  # to store raw dictionary data;
        self._entity_data = dict()  # to store entity attribute values;
        self._date_time_data = dict()  # to store date/time attribute values;
        # Process input data:
        for key, value in data.items():
            if value and type(value) == list:
                if self._entity_check(value[0]):
                    self._entity_data[key] = [eval(element['type'])(api=self.api, data=element) for element in value]
            else:
                if self._entity_check(data=value):
                    self._entity_data[key] = eval(value['type'])(api=self.api, data=value)
        self.date_time_sanity_check(attribute='created')
        self.date_time_sanity_check(attribute='updated')

    def __getattr__(self, name):
        """
        Overridden get attribute method.
        :param name: (str) attribute name;
        :return: a self._get call;
        """
        return self._get(name=name)

    def __getitem__(self, name):
        """
        Overridden get item method.
        :param name: (str) attribute name;
        :return: a self._get call;
        """
        return self._get(name=name)

    def __setitem__(self, name, value):
        """
        Overridden set attribute method - attempts to set the field value in the entity's raw data dictionary.
        :param name: (str) attribute name;
        :return: KeyError if the entity's raw data does not contain the given attribute key, None otherwise;
        """
        if name in self.fields:
            self.data[name] = value
        else:
            raise KeyError('Invalid entity field: {}'.format(name))

    def __repr__(self):
        """
        Represent the entity as a string.
        :return: a (str) entity representation;
        """
        display_data = self.data
        # Filter exclusive entity fields to display:
        if self.fields_show:
            display_data = {}
            for key, value in self.data.items():
                if key in self.fields_show:
                    display_data[key] = value
        return json.dumps(display_data, indent=4, sort_keys=True)

    def _get(self, name):
        """
        Get a given field value from the entity's raw data or request it from the Piper Server.
        :param name: the (str) name of the attribute;
        :return: an attribute value;
        """
        if name not in self.fields:
            return None
        elif name in self.data:
            value = self.data[name]
            if name in self._entity_data:
                value = self._entity_data[name]
            return value
        else:
            return self.get(attribute=name)

    @staticmethod
    def _entity_check(data):
        """
        Check if a given dictionary data represents an entity.
        :param data: dictionary data;
        :return: True/False;
        """
        if type(data) == dict and ('type' in data):
            return True
        return False

    @property
    def type(self):
        """
        A property to easily access this entity's class name, which also represents its type.
        :return: the name of this entity's class as a string.
        """
        return self.__class__.__name__

    @property
    def created(self):
        """
        String formatted created value.
        :return: date/time string;
        """
        return self.date_time_get(attribute='created')

    @property
    def updated(self):
        """
        String formatted updated value.
        :return: date/time string;
        """
        return self.date_time_get(attribute='updated')

    def get(self, attribute):
        """
        Make a GET Piper Server request to retrieve an attribute value.
        :param attribute: (str) an attribute name;
        :return: an attribute value;
        """
        LOGGER.debug(msg='Request (GET) for %s %s: "%s"' % (self.type, self.long_name, attribute))
        value = self.api.get(entity_type=self.type, entity_id=self.data['id'], attribute=attribute)
        # Store attribute value in the entity's raw data:
        self.data[attribute] = value
        # Process the value if it is compound:
        if value:
            if type(value) == list:
                first_element = value[0]
                if self._entity_check(data=first_element):
                    entity_model = first_element['type']
                    value = [eval(entity_model)(api=self.api, data=data) for data in value]
                    self._entity_data[attribute] = value
            elif self._entity_check(data=value):
                value = eval(value['type'])(api=self.api, data=value)
                self._entity_data[attribute] = value
            else:
                pass
        return value

    def update(self):
        """
        Update the entity.
        :return: the updated entity;
        """
        return self.api.update(self)

    def load(self):
        """
        Get a field value if the field is in the applicable entity fields.
        :return: None if the field is not in the entity fields, otherwise a field value;
        """
        for field in self.fields:
            self.get(field)

    def refresh(self):
        """
        Refresh the entity's raw data.
        :return: None;
        """
        self.data = self.api.get(entity_type=self.type, entity_id=self.data['id'])
        for attribute in dir(self):
            if attribute.startswith('_piper_'):
                setattr(self, attribute, None)

    def date_time_sanity_check(self, attribute):
        """
        Ensure that a date/time attribute value is stored and represented appropriately.
        :param attribute: the (str) name of the attribute;
        :return: None;
        """
        if attribute in self.data:
            self._date_time_data[attribute] = parse(self.data[attribute])

    def date_time_get(self, attribute):
        """
        Get a date/time attribute value as a string.
        :param attribute: the (str) attribute name;
        :return: a (str) date/time attribute value;
        """
        if attribute not in self._date_time_data:
            self.data[attribute] = self.get(attribute)
            self._date_time_data[attribute] = parse(self.data[attribute])
        return self._date_time_data[attribute]

    @property
    def publish_path(self):
        """
        Get the on-disk publish path of the entity.
        :return: a (str) folder/file path;
        """
        if not self._publish_path:
            self._publish_path = os.path.join(self.api.system.publish_root, self.get(attribute='publish_path'))
        return self._publish_path

    @property
    def work_path(self):
        """
        Get the on-disk work path of the entity.
        :return: a (str) folder/file path;
        """
        if not self._work_path:
            self._work_path = os.path.join(self.api.system.work_root, self.get(attribute='work_path'))
        return self._work_path

    @property
    def thumbnail(self):
        """
        Get the entity's thumbnail URL path.
        :return: a (str) thumbnail URL;
        """
        if not self._thumbnail:
            self._thumbnail = os.environ['PIPER_SERVER'] + self.get(attribute='thumbnail_url')
        return self._thumbnail

    @property
    def thumbnail_spec(self):
        """
        Get the entity's thumbnail spec URL path.
        :return: a (str) thumbnail URL;
        """
        if not self._thumbnail_spec:
            self._thumbnail_spec = os.environ['PIPER_SERVER'] + self.get(attribute='thumbnail_spec_url')
        return self._thumbnail_spec

    @property
    def url(self):
        if not self._url:
            if 'full_url' in self.data:
                url = self.data['full_url']
            else:
                url = self.get(attribute='full_url')
            if url:
                self._url = self.api.absolute_url(url=url)
        return self._url


class GlobalsConfig(Entity):
    fields = ['type', 'id', 'name', 'start_frame', 'email_on_update', 'email_host', 'email_port', 'email_username',
              'email_password', 'ticket_email', 'shotgun_sync', 'shotgun_url', 'shotgun_script_name',
              'shotgun_application_key', 'can_sg_sync', 'email_on_publish', 'email_ready', 'auto_update_thumbnails']

    def __init__(self, **kwargs):
        super(GlobalsConfig, self).__init__(**kwargs)


class Tag(Entity):
    fields = ['type', 'id', 'name', 'link_id', 'metadata', 'created', 'created_by']

    def __init__(self, **kwargs):
        super(Tag, self).__init__(**kwargs)


class History(Entity):
    fields = ['type', 'id', 'before', 'after', 'created', 'created_by']

    def __init__(self, **kwargs):
        super(History, self).__init__(**kwargs)


class Status(Entity):
    fields = BASE_FIELDS

    def __init__(self, **kwargs):
        super(Status, self).__init__(**kwargs)


class StatusMixin(object):
    fields = BASE_FIELDS

    def __init__(self, **kwargs):
        super(StatusMixin, self).__init__(**kwargs)


class Department(Entity):
    fields = BASE_FIELDS + ['email', 'status']

    def __init__(self, **kwargs):
        super(Department, self).__init__(**kwargs)


class Project(Entity):
    fields = BASE_FIELDS + ['episodic', 'status', 'assets', 'sequences', 'episodes', 'publish_path_template',
                            'work_path_template', 'file_name_template']

    def __init__(self, **kwargs):
        super(Project, self).__init__(**kwargs)

    def tree(self):
        """
        Get the entire project tree as a dictionary.
        :return: a project data tree dictionary;
        """
        return self.api.request('project/{}/tree'.format(self.long_name))


class User(Entity):
    fields = BASE_FIELDS + ['department', 'projects', 'is_human', 'status', 'tasks', 'is_superuser', 'active_tasks',
                            'email', 'username']

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)


class Episode(Entity):
    fields = BASE_FIELDS + ['project', 'status']

    def __init__(self, **kwargs):
        super(Episode, self).__init__(**kwargs)


class Sequence(Entity):
    fields = BASE_FIELDS + ['project', 'episode', 'shots', 'status']

    def __init__(self, **kwargs):
        super(Sequence, self).__init__(**kwargs)


class AssetType(Entity):
    fields = BASE_FIELDS

    def __init__(self, **kwargs):
        super(AssetType, self).__init__(**kwargs)


class Category(Entity):
    fields = BASE_FIELDS

    def __init__(self, **kwargs):
        super(Category, self).__init__(**kwargs)


class Asset(Entity):
    fields = BASE_FIELDS + ['asset_type', 'project', 'episode', 'status', 'sequence', 'tasks', 'resources']

    def __init__(self, **kwargs):
        super(Asset, self).__init__(**kwargs)


class Shot(Entity):
    fields = BASE_FIELDS + ['project', 'episode', 'sequence', 'status', 'resources', 'duration', 'pre_roll',
                            'post_roll']

    def __init__(self, **kwargs):
        super(Shot, self).__init__(**kwargs)


class Step(Entity):
    fields = BASE_FIELDS + ['category', 'level']

    def __init__(self, **kwargs):
        super(Step, self).__init__(**kwargs)


class Task(Entity):
    _link = None
    fields = BASE_FIELDS + ['step', 'project', 'episode', 'asset', 'shot', 'link', 'resources',
                            'artists', 'start_date', 'end_date', 'status', 'is_active']

    def __init__(self, **kwargs):
        super(Task, self).__init__(**kwargs)
        self.date_time_sanity_check(attribute='start_date')
        self.date_time_sanity_check(attribute='end_date')

    @property
    def start_date(self):
        return self.date_time_get(attribute='start_date')

    @property
    def end_date(self):
        return self.date_time_get(attribute='end_date')

    @property
    def link(self):
        if not self._link:
            self._link = self.asset or self.shot
        return self._link


class Resource(Entity):
    fields = BASE_FIELDS + ['project', 'source', 'task', 'artist', 'status', 'category', 'versions', 'link']

    def __init__(self, **kwargs):
        super(Resource, self).__init__(**kwargs)


class Version(Entity):
    fields = BASE_FIELDS + ['project', 'resource', 'artist', 'status', 'files', 'snapshots', 'work_file', 'is_latest',
                            'preview', 'for_dailies']

    def __init__(self, **kwargs):
        super(Version, self).__init__(**kwargs)

    @property
    def snapshots(self):
        """
        Get a dictionary containing the name and full path of each snapshot for this version.
        :return: dict;
        """
        snapshot_dict = {}
        if self.work_file and self.work_file.work_path:
            snapshot_path = os.path.join(os.path.dirname(self.work_file.work_path), '_snapshots')
            if os.path.isdir(snapshot_path):
                snapshots = os.listdir(snapshot_path)
                snapshots.sort(reverse=True)
                for snapshot in snapshots:
                    # Store snapshot in the context data dictionary for future reference:
                    snapshot_dict[snapshot] = os.path.join(snapshot_path, snapshot)
        return snapshot_dict


class Dependency(Entity):
    fields = ['type', 'id', 'name', 'dependant_model', 'dependant_id', 'created_by']
    _dependant = None
    _dependencies = []

    def __init__(self, **kwargs):
        super(Dependency, self).__init__(**kwargs)

    @property
    def dependant(self):
        """
        Get this dependency entity's dependant entity (parent entity).
        :return: an entity;
        """
        if not self._dependant:
            self._dependant = self.api.get(entity_type=self.dependant_model, entity_id=self.dependant_id)
        return self._dependant

    @dependant.setter
    def dependant(self, value):
        """
        Set this dependency entity's dependant entity (parent entity).
        :param value: an entity;
        :return: None;
        """
        if not isinstance(value, Entity):
            msg = 'Dependant must be a valid entity.'
            LOGGER.error(msg)
            raise RuntimeError(msg)
        self.data['dependant_id'] = value.id
        self.data['dependant_model'] = value.type
        self._dependant = value

    @property
    def dependencies(self):
        """
        Get this dependency entity's dependencies (child entities).
        :return: a list of entities;
        """
        if not self._dependencies:
            dependencies = self.get(attribute='dependencies')
            if dependencies:
                for entity_type, ids in dependencies.items():
                    self._dependencies += [self.api.get(entity_type=entity_type, entity_id=id) for id in ids]
        return self._dependencies

    @dependencies.setter
    def dependencies(self, value):
        """
        Set this dependency entity's dependencies (child entities).
        :param value: a list of entities;
        :return: None;
        """
        dependencies = {}
        if value and type(value) == list:
            for entity in value:
                if not isinstance(entity, Entity):
                    dependencies = {}
                    break
                if entity.type == self.type and entity.id == self.id:
                    continue
                if entity.type in dependencies and entity.id not in dependencies[entity.type]:
                    dependencies[entity.type].append(entity.id)
                else:
                    dependencies[entity.type] = [entity.id]
            if dependencies:
                self.data['dependencies'] = dependencies
                self._dependencies = value
                return
        msg = 'Dependencies must be a list of entities.'
        LOGGER.error(msg)
        raise RuntimeError(msg)

    def append(self, entity, commit=True):
        """
        Append entity to dependencies.
        :param entity: an entity to add to dependencies;
        :param commit: whether to commit the changes to the database;
        :return: None;
        """
        if entity.type in self.data['dependencies'] and entity.id not in self.data['dependencies'][entity.type]:
            self.data['dependencies'][entity.type].append(entity.id)
            self._dependencies.append(entity)
            if commit:
                self.update()

    def remove(self, entity, commit=True):
        """
        Remove entity from dependencies.
        :param entity: an entity to remove from dependencies;
        :param commit: whether to commit the changes to the database;
        :return: None;
        """
        if entity.type in self.dependencies and entity.id in self.dependencies[entity.type]:
            self.dependencies[entity.type].remove(entity.id)
            for dependency in self._dependencies:
                if dependency['id'] == entity['id']:
                    self._dependencies.remove(dependency)
                    if commit:
                        self.update()
                    break

    def replace(self, old_entity, new_entity, commit=True):
        """
        Replace one entity with another in the dependencies.
        :param old_entity: an entity to replace;
        :param new_entity: a new entity to replace the old entity with;
        :param commit: whether to commit the changes to the database;
        :return: None;
        """
        self.remove(entity=old_entity, commit=False)
        self.append(entity=new_entity, commit=False)
        if commit:
            self.update()


class File(Entity):
    fields = BASE_FIELDS + ['format', 'is_sequence', 'is_image', 'is_video', 'is_preview', 'version', 'artist',
                            'status', 'range', 'resource', 'task', 'shot', 'sequence', 'asset', 'project']

    def __init__(self, **kwargs):
        super(File, self).__init__(**kwargs)


class Config(Entity):
    fields = BASE_FIELDS + ['config_rules', 'inherited_config_rules', 'status']

    def __init__(self, **kwargs):
        super(Config, self).__init__(**kwargs)


class WorkLog(Entity):
    fields = BASE_FIELDS + ['project', 'episode', 'task', 'artist', 'duration']

    def __init__(self, **kwargs):
        super(WorkLog, self).__init__(**kwargs)


class System(Entity):
    fields = ['type', 'id', 'name', 'long_name', 'platform', 'publish_root', 'work_root', 'software_root',
              'modules_root']
    _modules_root = None

    def __init__(self, **kwargs):
        super(System, self).__init__(**kwargs)

    @property
    def modules_root(self):
        """
        Get this system entity's modules root and format its {piper} keyword if applicable.
        :return: modules root path as string;
        """
        if not self._modules_root:
            self._modules_root = self.get(attribute='modules_root').format(piper=PIPER)
        return self._modules_root


class Software(Entity):
    fields = BASE_FIELDS + ['category', 'system', 'executable', 'file_extensions', 'command_line_renderer',
                            'render_formats', 'module_environment_variable']

    def __init__(self, **kwargs):
        super(Software, self).__init__(**kwargs)


class Module(Entity):
    fields = BASE_FIELDS + ['software', 'root']
    _root = None

    def __init__(self, **kwargs):
        super(Module, self).__init__(**kwargs)

    @property
    def root(self):
        """
        Get this module entity's root and format its {piper} keyword if applicable.
        :return: root path as string;
        """
        if not self._root:
            self._root = self.get(attribute='root').format(piper=PIPER)
        return self._root


class Profile(Entity):
    fields = BASE_FIELDS + ['software', 'modules']

    def __init__(self, **kwargs):
        super(Profile, self).__init__(**kwargs)


class Playlist(Entity):
    fields = BASE_FIELDS + ['project', 'episode', 'version_ids']

    def __init__(self, **kwargs):
        super(Playlist, self).__init__(**kwargs)


class Note(Entity):
    fields = ['project', 'type', 'id', 'content', 'created', 'author', 'mentioned_users', 'tags', 'created_by']

    def __init__(self, **kwargs):
        super(Note, self).__init__(**kwargs)


class TaskTemplate(Entity):
    fields = BASE_FIELDS + ['tasks']

    def __init__(self, **kwargs):
        super(TaskTemplate, self).__init__(**kwargs)


class EditType(Entity):
    fields = BASE_FIELDS

    def __init__(self, **kwargs):
        super(EditType, self).__init__(**kwargs)


class Edit(Entity):
    fields = BASE_FIELDS + ['edit_type', 'duration', 'frames', 'project', 'episode',
                            'timecode_start', 'timecode_end', 'fps', 'edl', 'items']

    def __init__(self, **kwargs):
        super(Edit, self).__init__(**kwargs)


class Attachment(Entity):
    fields = ['project', 'type', 'id', 'file', 'author', 'created', 'created_by']

    def __init__(self, **kwargs):
        super(Attachment, self).__init__(**kwargs)
