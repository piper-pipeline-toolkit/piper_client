# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import nuke
import nukescripts
from tempfile import gettempdir
from piper_client.core.dcc import PiperDccApi as BaseApi
from piper_client.core.logger import get_logger

LOGGER = get_logger('Piper Client Nuke API')


class PiperDccApi(BaseApi):
    """
    Nuke Piper API.
    """
    
    name = 'nuke'
    src = nuke
    render_flags = '-t -m 10 -X "{RENDER_NODE}" -F {START_FRAME}-{END_FRAME}x{BY_FRAME} {SCENE}'
    render_node = None

    def __init__(self, **kwargs):
        super(PiperDccApi, self).__init__(**kwargs)

    def open_file(self, path_to_file):
        """
        Open a specific file.
        :param path_to_file: the full path to the file to open;
        :return: the full name of the opened file as a string if it exists, otherwise - None;
        """
        if os.path.isfile(path_to_file):
            LOGGER.debug('Opening file %s' % path_to_file)
            nuke.scriptOpen(path_to_file)
            return path_to_file
        else:
            LOGGER.debug('Could not find file %s' % path_to_file)
            return None

    def save_file(self, file_path=None):
        """
        Save the current file.
        :param file_path: the path to save the file to;
        :return: the full name of the saved file as a string;
        """
        if not file_path:
            file_path = self.get_current_file_name()
            if not file_path:
                file_path = os.path.join(gettempdir(), 'temp.nk')
        # Create parent directory if it doesn't exist:
        parent_dir = os.path.dirname(file_path)
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir)
        try:
            LOGGER.debug('Saving file %s' % file_path)
            nuke.scriptSaveAs(file_path)
            return file_path
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to save file %s: %s' % (file_path, e))
            return False

    def export_selection(self, file_path):
        """
        Export the current selection to the given fil path.
        :param file_path: the path to export the file to;
        :return: the full name of the saved file as a string;
        """
        try:
            LOGGER.debug('Exporting selection to file %s' % file_path)
            nuke.nodeCopy(file_path)
            return file_path
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to export the current selection to file %s: %s' % (file_path, e))
            return False

    def snapshot(self):
        """
        Save the current file as a snapshot file.
        :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
        """
        snap = super(PiperDccApi, self).snapshot()
        if snap:
            nuke.message('Piper - snapshot created!')
        else:
            nuke.message('Piper - the current file is invalid for a snapshot.')
        return snap

    def get_current_file_name(self):
        """
        Get the full path of the file currently opened.
        :return: the full path of the current file (string);
        """
        try:
            return nuke.scriptName()
        except RuntimeError:
            return None

    def get_current_file_format(self):
        """
        Get the current file's format.
        :return: a (str) file format;
        """
        file_name = self.get_current_file_name()
        if not file_name:
            file_format = 'nk'
        else:
            file_format = file_name.split('.')[-1]
        return file_format

    def find_node_from_id(self, id_attribute, id_value, nodes=None):
        """
        Find a node in a list of nodes, given a ID attribute and its value.
        :param id_attribute: the (str) ID attribute to look for;
        :param id_value: the value of the ID attribute to match;
        :param nodes: a list of nodes to look through;
        :return: a node with a matching attribute/value pair if it exists, otherwise None;
        """
        # If no nodes specified, recurse through all group nodes:
        if not nodes:
            nodes = nuke.allNodes('Group', recurseGroups=True)
        matching_node = None
        for node in nodes:
            # Look for the attribute and check its value:
            if node.knob(id_attribute):
                if node[id_attribute].value() == id_value:
                    matching_node = node
                    break

        return matching_node

    def select(self, node):
        """
        Select a given node.
        :param node: a scene node;
        :return: None;
        """
        self.clear_selection()
        node.setSelected(True)

    def select_all(self):
        """
        Select all nodes.
        :return: None;
        """
        nuke.selectAll()
        for viewer_node in nuke.allNodes('Viewer'):
            viewer_node.setSelected(False)

    def add_to_selection(self, node):
        """
        Add the a given node to the current selection.
        :param node: a scene node;
        :return: None;
        """
        node.setSelected(True)

    def get_selection(self):
        """
        Get a list of the currently selected nodes.
        :return: a list of nodes;
        """
        return nuke.selectedNodes()

    def clear_selection(self):
        """
        Clear all selection in the scene.
        :return: None;
        """
        nukescripts.clear_selection_recursive()

    def ungroup(self, group):
        """
        Delete or ungroup a given group node name.
        :param group: a (str) group node name;
        :return: None
        """
        group_node = nuke.toNode(group)
        group_node.setSelected(True)
        nuke.expandSelectedGroup()  # versions are only grouped in publish files;

    def create_group(self):
        """
        Group nodes together into a single node.
        :return: a group node;
        """
        group = nuke.collapseToGroup()
        return group

    def tag_publish_node(self, node, file_entity):
        """
        Set the publish group node's ID attributes to correspond to the relevant file entity.
        :param node: the group node;
        :param file_entity: the published file entity;
        :return: :return: the tagged publish node;
        """
        file_id_knob = nuke.nuke.Int_Knob('piper_file_id', 'Piper File ID')
        file_id_knob.setVisible(False)
        node.addKnob(file_id_knob)
        node['piper_file_id'].setValue(file_entity.id)
        node['name'].setValue(file_entity.name)
        node['lock_connections'].setValue(True)  # don't allow editing the nodes inside the published file;
        return node

    def add_published_file(self, file_entity, deactivate_nodes=True):
        """
        Add a published file to the scene.
        :param file_entity: the published file entity to add;
        :param deactivate_nodes: whether to deactivate all active group nodes (Nuke-specific);
        :return: None;
        """
        if deactivate_nodes:
            self._deactivate_nodes()
        nukescripts.clear_selection_recursive()
        if file_entity.format not in self.software.file_extensions:
            if file_entity.range:
                read_node = nuke.createNode('Read')
                read_node.knob('file').fromUserText('{0} {1}-{2}'.format(file_entity.publish_path,
                                                                         file_entity.range[0],
                                                                         file_entity.range[1]))
            else:
                read_node = nuke.nodes.Read(file=file_entity.publish_path)
            read_node.setSelected(True)
            group = self.create_group()
            self.tag_publish_node(node=group, file_entity=file_entity)
            return group
        else:
            return nuke.nodePaste(file_entity.publish_path)

    def find_entity_nodes(self, root=None):
        """
        Find all nodes representing entities in the scene under a given root.
        :param root: a root entity node to start the search from;
        :return: a dictionary of published file ID values for the given root key;
        """
        found_ids = {}
        # Get the relevant nodes to look through:
        if root:
            root_node = self.find_node_from_id(id_attribute='piper_file_id',
                                               id_value=root.id,
                                               nodes=nuke.allNodes('Group', recurseGroups=True))
            if not root_node:
                return found_ids
            root_node.begin()

        nodes = nuke.allNodes('Group')
        if root:
            root_node.end()

        # Recursively find all entity nodes:
        for node in nodes:
            if node.knob('piper_file_id'):
                node.begin()
                found_ids[int(node['piper_file_id'].value())] = self.find_entity_nodes()
                node.end()

        return found_ids

    def remove_published_file(self, file_entity):
        """
        Remove a published file from the scene.
        :param file_entity: the file entity representing the file to be removed;
        :return: None;
        """
        node = nuke.toNode(file_entity.name)
        if node:
            nuke.delete(node)

    def _deactivate_nodes(self):
        """
        Make sure there are no active nodes.
        :return: None;
        """
        for node in nuke.allNodes('Group', recurseGroups=True):
            node.end()
        nukescripts.clear_selection_recursive()

    def replace_published_file(self, previous_file_entity, new_file_entity):
        """
        Replace an existing published file node with a new published file node.
        :param previous_file_entity: the version file entity of the previously selected version;
        :param new_file_entity: the newly selected file entity be added;
        :return: True if successful, otherwise False;
        """
        self._deactivate_nodes()

        # Find the target node to replace:
        target_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=previous_file_entity.id)
        if not target_node:
            return False

        # Find the parent entity, under which the replacement will happen:
        parent_node = None
        target_node_full_name = target_node.fullName()
        target_node_sub_name = '.{}'.format(target_node.name())
        if target_node_sub_name in target_node_full_name:
            parent_node = nuke.toNode(target_node_full_name.replace(target_node_sub_name, ''))
            parent_node.begin()

        # Copy the position, inputs and outputs of the old published file node to the new published file node:
        self.add_published_file(new_file_entity, deactivate_nodes=False)
        new_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=new_file_entity.id)
        target_position = (target_node.xpos(), target_node.ypos())
        input_nodes = []
        output_nodes = []
        for i in range(target_node.inputs()):
            input_nodes.append((i, target_node.input(i)))

        for dependant_node in nuke.dependentNodes(nuke.INPUTS | nuke.HIDDEN_INPUTS, target_node):
            for i in range(dependant_node.inputs()):
                if dependant_node.input(i) == target_node:
                    output_nodes.append((i, dependant_node))
                    dependant_node.setInput(i, None)
        new_node['xpos'].setValue(target_position[0])
        new_node['ypos'].setValue(target_position[1])
        nuke.delete(target_node)

        for input_node in input_nodes:
            new_node.setInput(input_node[0], input_node[1])
        for output_node in output_nodes:
            output_node[1].setInput(output_node[0], new_node)

        if parent_node:
            parent_node.end()
        return True

    def get_render_start_frame(self):
        """
        Get the start frame value as set in the render settings.
        :return: (float) start frame;
        """
        if self.render_node:
            self.render_node['first'].value()

    def get_render_end_frame(self):
        """
        Get the end frame value as set in the render settings.
        :return: (float) end frame;
        """
        if self.render_node:
            self.render_node['last'].value()

    def set_render_start_frame(self, value):
        """
        Set the render start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        if self.render_node:
            self.render_node['use_limit'].setValue(True)
            self.render_node['first'].setValue(value)

    def set_render_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        if self.render_node:
            self.render_node['use_limit'].setValue(True)
            self.render_node['last'].setValue(value)

    def get_playback_start_frame(self):
        """
        Get the start frame value as set in the current scene playback range.
        :return: (float) start frame;
        """
        return nuke.Root()['first_frame'].value()

    def get_playback_end_frame(self):
        """
        Get the end frame value as set in the current scene playback range.
        :return: (float) end frame;
        """
        return nuke.Root()['last_frame'].value()

    def set_playback_start_frame(self, value):
        """
        Set the playback start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        nuke.Root()['first_frame'].setValue(value)

    def set_playback_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        nuke.Root()['last_frame'].setValue(value)

    def get_image_format(self):
        """
        Get the output image format as set in the render settings.
        :return: (str) image format or empty string if unavailable;
        """
        if self.render_node:
            return self.render_node['file_type'].value()
        return ''

    def get_fps(self):
        """
        Get the current FPS value.
        :return: (float) frames per second value;
        """
        return nuke.Root()['fps'].value()

    def set_fps(self, value):
        """
        Set the scene's FPS to the given value.
        :param value: the FPS number value;
        :return: None;
        """
        nuke.Root()['fps'].setValue(value)

    def set_render_path(self, path):
        """
        Set a specific render path in the render settings.
        :param path: a (str) path;
        :return: None;
        """
        if self.render_node:
            render_node = nuke.toNode(self.render_node)
            render_node['file'].setValue(path)

    def list_nodes(self, node_type='Write'):
        """
        List the full names of all scene nodes of a certain type, the default being render node type.
        :param node_type: the (str) type of node to list;
        :return: a list of node names;
        """
        return [node.fullName() for node in nuke.allNodes(node_type)]

    def render_setup(self, file_entity, render_node=None):
        """
        Setup the current file so that it's ready for rendering.
        :param file_entity: the current file entity;
        :param render_node: a render node to use in the render setup instead of creating a new one;
        :return: None;
        """
        if render_node:
            self.render_node = render_node

        if not self.render_node:
            self.select_all()
            group = self.create_group()
            group['name'].setValue('{}_render_group'.format(file_entity.name))
            render_node = nuke.createNode('Write')
            render_node['name'].setValue('piper_render_node')
            render_node.setInput(0, group)
            self.render_node = render_node.fullName()

        self.set_render_path(path=file_entity.work_path)
