# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import hou
from tempfile import gettempdir
from piper_client.core.dcc import PiperDccApi as BaseApi
from piper_client.core.logger import get_logger

LOGGER = get_logger('Piper Houdini API')


class PiperDccApi(BaseApi):
    """
    Houdini Piper API.
    """

    name = 'houdini'
    src = hou
    render_flags = '-d {RENDER_NODE} -f {START_FRAME} {END_FRAME} -i {BY_FRAME} {SCENE}'
    render_node = None

    def __init__(self, **kwargs):
        super(PiperDccApi, self).__init__(**kwargs)

    def open_file(self, path_to_file):
        """
        Open a specific file.
        :param path_to_file: the full path to the file to open;
        :return: the full name of the opened file as a string if it exists, otherwise - None;
        """
        if os.path.isfile(path_to_file):
            LOGGER.debug('Opening file %s' % path_to_file)
            hou.hipFile.load(path_to_file, suppress_save_prompt=True)
            return path_to_file
        else:
            LOGGER.debug('Could not find file %s' % path_to_file)
            return None

    def save_file(self, file_path=None):
        """
        Save a file to a specified path.
        :param file_path: the path to save the file to;
        :return: the full name of the saved file as a string;
        """
        if not file_path:
            file_path = self.get_current_file_name()
            if not file_path:
                file_path = os.path.join(gettempdir(), 'temp.hip')
        try:
            LOGGER.debug('Saving file %s' % file_path)
            hou.hipFile.save(file_name=file_path)
            return file_path
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to save file %s: %s' % (file_path, e))
            return False

    def export_selection(self, file_path):
        """
        Export the current selection to the given fil path.
        :param file_path: the path to export the file to;
        :return: the full name of the saved file as a string;
        """
        obj = hou.node('/obj/')
        selection = self.get_selection()
        try:
            LOGGER.debug('Exporting selection to file %s' % file_path)
            return obj.saveChildrenToFile(selection, '', file_path)
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to export the current selection to file %s: %s' % (file_path, e))
            return False

    def snapshot(self):
        """
        Save the current file as a snapshot file.
        :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
        """
        snap = super(PiperDccApi, self).snapshot()
        if snap:
            hou.ui.displayMessage('Snapshot created!', title='Piper Snapshot')
        else:
            hou.ui.displayMessage('The current file is invalid for a snapshot!',
                                  title='Piper Snapshot',
                                  severity=hou.severityType.Error)
        return snap

    def get_current_file_name(self):
        """
        Get the full path of the file currently opened.
        :return: the full path of the current file (string);
        """
        return hou.hipFile.name()

    def get_current_file_format(self):
        """
        Get the current file's format.
        :return: a (str) file format;
        """
        file_name = self.get_current_file_name()
        if not file_name:
            file_format = 'hip'
        else:
            file_format = file_name.split('.')[-1]
        return file_format

    def find_node_from_id(self, id_attribute, id_value, nodes=None):
        """
        Find a node in a list of nodes, given a ID attribute and its value.
        :param id_attribute: the (str) ID attribute to look for;
        :param id_value: the value of the ID attribute to match;
        :param nodes: a list of nodes to look through;
        :return: a node with a matching attribute/value pair if it exists, otherwise None;
        """
        # If no nodes specified, recurse through all group nodes:
        if not nodes:
            obj_node = hou.node('/obj')
            nodes = obj_node.allSubChildren()
        matching_node = None
        for node in nodes:
            if node.type().name() == 'subnet':
                # Look for the attribute and check its value:
                if node.userData(id_attribute) and node.userData(id_attribute) == str(id_value):
                    matching_node = node
                    break
        return matching_node

    def select(self, node):
        """
        Select a given node.
        :param node: a scene node;
        :return: None;
        """
        node.setSelected(True, clear_all_selected=True)

    def select_all(self):
        """
        Select all nodes.
        :return: None;
        """
        obj_node = hou.node('/obj')
        for node in obj_node.children():
            node.setSelected(True)

    def add_to_selection(self, node):
        """
        Add the a given node to the current selection.
        :param node: a scene node;
        :return: None;
        """
        node.setSelected(True)

    def get_selection(self):
        """
        Get a list of the currently selected nodes.
        :return: a list of nodes;
        """
        return hou.selectedNodes()

    def clear_selection(self):
        """
        Clear all selection in the scene.
        :return: None;
        """
        hou.clearAllSelected()

    def ungroup(self, group):
        """
        Delete or ungroup a given group node name.
        :param group: a (str) group node name;
        :return: None
        """
        grp_node = hou.node('/obj/{}'.format(group))
        grp_children = grp_node.children()
        hou.moveNodesTo(grp_children, grp_node.parent())
        grp_node.destroy()

    def create_group(self):
        """
        Group nodes together into a single node.
        :return: a group node;
        """
        # Group the nodes and tag the group with the file entity ID:
        selection = self.get_selection()
        obj_node = hou.node('/obj')
        group = obj_node.createNode('subnet', 'piper_publish_grp')
        hou.moveNodesTo(selection, group)
        return group

    def tag_publish_node(self, node, file_entity):
        """
        Set the publish group node's ID attributes to correspond to the relevant file entity.
        :param node: the group node;
        :param file_entity: the published file entity;
        :return: :return: the tagged publish node;
        """
        node_path = node.path()
        for attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            hou.Parm.lock(hou.parm('{0}/{1}'.format(node_path, attr)), True)
        node.setName(file_entity.name)
        node.setUserData('piper_file_id', str(file_entity.id))
        return node

    def add_published_file(self, file_entity):
        """
        Add a published file to the scene.
        :param file_entity: the published file entity to add;
        :return: None;
        """
        obj_node = hou.node('/obj')
        existing_nodes = obj_node.children()
        obj_node.loadChildrenFromFile(file_entity.publish_path)
        if file_entity.format not in self.software.file_extensions:
            new_nodes = [n for n in obj_node.children() if n not in existing_nodes]

            entity_node = obj_node.createNode('subnet')

            entity_node.setName('{0}_{1}'.format(file_entity.version.resource.task.link.long_name,
                                                 file_entity.version.resource.task.long_name))

            hou.moveNodesTo(new_nodes, entity_node)
            net_editor = hou.ui.paneTabOfType(hou.paneTabType.NetworkEditor)
            net_editor.cd('/obj')
            entity_node.moveToGoodPosition()

            self.tag_publish_node(node=entity_node, file_entity=file_entity)

    def find_entity_nodes(self, root=None, root_nodes=None):
        """
        Find all nodes representing entities in the scene under a given root.
        :param root: a root entity node to start the search from;
        :param root_nodes: a list of root entity nodes to start the search from;
        :return: a dictionary of published file ID values for the given root key;
        """
        found_ids = {}
        if root and not root_nodes:
            root_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(root.id))
            if not root_node:
                return found_ids
        elif not root and not root_nodes:
            root_node = hou.node('/obj')
        root_nodes = root_node.children()

        for node in root_nodes:
            if node.type().name() == 'subnet' and node.userData('piper_file_id'):
                found_ids[int(node.userData('piper_file_id'))] = self.find_entity_nodes(root_nodes=node.children())
        return found_ids

    def remove_published_file(self, file_entity):
        """
        Remove a published file from the scene.
        :param file_entity: the file entity representing the file to be removed;
        :return: None;
        """
        node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(file_entity.id))
        if node:
            node.destroy()

    def replace_published_file(self, previous_file_entity, new_file_entity):
        """
        Replace an existing published file node with a new published file node.
        :param previous_file_entity: the version file entity of the previously selected version;
        :param new_file_entity: the newly selected file entity be added;
        :return: True if successful, otherwise False;
        """

        old_entity_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(previous_file_entity.id))
        if not old_entity_node:
            return False

        self.add_published_file(file_entity=new_file_entity)
        new_entity_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(new_file_entity.id))
        new_node_name = new_entity_node.name()
        parent = old_entity_node.parent()
        hou.moveNodesTo([new_entity_node], parent)
        new_entity_node = hou.node('{0}/{1}'.format(parent.path(), new_node_name))

        # Store inputs and outputs for re-creation:
        input_dict = dict()
        for node_connection in old_entity_node.inputConnections():
            input_dict[node_connection.inputIndex()] = node_connection.inputItem()
        output_dict = dict()
        for node_connection in old_entity_node.outputConnections():
            output_dict[node_connection.outputIndex()] = [node_connection.outputItem(), node_connection.inputIndex()]
        old_entity_node.destroy()

        # Recreate inputs and outputs:
        for index in input_dict:
            new_entity_node.setInput(index, input_dict[index])

        for index in output_dict:
            node = output_dict[index][0]
            input_index = output_dict[index][1]
            node.setInput(input_index, new_entity_node)

        self.tag_publish_node(node=new_entity_node, file_entity=new_file_entity)
        new_entity_node.moveToGoodPosition()
        return True

    def get_render_start_frame(self):
        """
        Get the start frame value as set in the render settings.
        :return: (float) start frame;
        """
        if self.render_node:
            render_node = hou.node(self.render_node)
            render_node.parm('f1').eval()

    def get_render_end_frame(self):
        """
        Get the end frame value as set in the render settings.
        :return: (float) end frame;
        """
        if self.render_node:
            render_node = hou.node(self.render_node)
            render_node.parm('f2').eval()

    def set_render_start_frame(self, value):
        """
        Set the render start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        if self.render_node:
            render_node = hou.node(self.render_node)
            render_node.parm('trange').set(1)
            render_node.parm('f1').deleteAllKeyframes()
            render_node.parm('f1').set(value)

    def set_render_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        if self.render_node:
            render_node = hou.node(self.render_node)
            render_node.parm('trange').set(1)
            render_node.parm('f2').deleteAllKeyframes()
            render_node.parm('f2').set(value)

    def get_playback_start_frame(self):
        """
        Get the start frame value as set in the current scene playback range.
        :return: (float) start frame;
        """
        return hou.playbar.frameRange()[0]

    def get_playback_end_frame(self):
        """
        Get the end frame value as set in the current scene playback range.
        :return: (float) end frame;
        """
        return hou.playbar.frameRange()[1]

    def set_playback_start_frame(self, value):
        """
        Set the playback start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        end_frame = self.get_playback_end_frame()
        if end_frame < value:
            end_frame = value + 10
        hou.playbar.setFrameRange(value, end_frame)

    def set_playback_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        start_frame = self.get_playback_start_frame()
        if start_frame > value:
            start_frame = value - 10
        hou.playbar.setFrameRange(start_frame, value)

    def get_image_format(self):
        """
        Get the output image format as set in the render settings.
        :return: (str) image format or empty string if unavailable;
        """
        pass

    def get_fps(self):
        """
        Get the current FPS value.
        :return: (float) frames per second value;
        """
        return hou.fps()

    def set_fps(self, value):
        """
        Set the scene's FPS to the given value.
        :param value: the FPS number value;
        :return: None;
        """
        hou.setFps(value)

    def set_render_path(self, path):
        """
        Set a specific render path in the render settings.
        :param path: a (str) path;
        :return: None;
        """
        if self.render_node:
            render_node = hou.node(self.render_node)
            if render_node.parm('vm_picture'):
                render_node.parm('vm_picture').set(path)
            if render_node.parm('dopoutput'):
                render_node.parm('dopoutput').set(path)

    def list_nodes(self, node_type='render'):
        """
        List the full names of all scene nodes of a certain type, the default being render node type.
        :param node_type: the (str) type of node to list;
        :return: a list of node names;
        """
        if node_type == 'render':
            result = self.list_nodes(node_type='ifd')
            result += self.list_nodes(node_type='dop')
            return result
        return [node.path() for node in hou.node('/').allSubChildren() if x.type().name() == node_type]

    def render_setup(self, file_entity, render_node=None):
        """
        Setup the current file so that it's ready for rendering.
        :param file_entity: the current file entity;
        :param render_node: a render node to use in the render setup instead of creating a new one;
        :return: None;
        """
        if render_node:
            self.render_node = render_node

        if not self.render_node:
            out_node = hou.node('/out')
            render_node = out_node.createNode('ifd', 'piper_render_node')
            render_node.parm('trange').set(1)
            self.render_node = render_node.path()
            self.set_render_start_frame(self.get_playback_start_frame())
            self.set_render_end_frame(self.get_playback_end_frame())
        self.set_render_path(path=file_entity.work_path)
