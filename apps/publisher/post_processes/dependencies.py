# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from . import PostProcess as PostProcessBase


class PostProcess(PostProcessBase):

    name = 'Dependencies'
    description = 'Record dependencies for this version.'
    required = True
    checked = True
    priority = 99

    def __init__(self, **kwargs):
        super(PostProcess, self).__init__(**kwargs)

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the process.
        :return: True/False;
        """
        # Find entities present in the scene and create a dependency based on them:
        dependency_ids = self.dcc_api.find_entity_nodes().keys()
        if dependency_ids:
            # Create version dependency:
            version_dependency = self.piper_api.create(entity_type='Dependency',
                                                       dependant_id=self.version.id,
                                                       dependant_model=self.version.type)
            # Create files dependency:
            file_entity = self.version.work_file

            file_dependency = self.piper_api.create(entity_type='Dependency',
                                                    dependant_id=file_entity.id,
                                                    dependant_model=file_entity.type)

            self.piper_api.query_fields = ['version']
            self.piper_api.query_fields_reset = False
            file_dependency.dependencies = [self.piper_api.get(entity_type='File', entity_id=id)
                                            for id in dependency_ids]
            file_dependency.update()
            self.piper_api.query_fields = []
            self.piper_api.query_fields_reset = True
            # Update the version dependency based on the file dependency files:
            version_dependency.dependencies = [file.version for file in file_dependency.dependencies]
            version_dependency.update()
            self.logger.info('Recorded version dependencies.')
        return True
