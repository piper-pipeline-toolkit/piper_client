# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import importlib
from piper_client.core.logger import get_logger


def get_post_processes():
    """
    Load an return a DCC-related API module.
    :param dcc: the name of the related DCC;
    :return: a module object, e.g. core.dcc.maya_api;
    """
    modules = []
    for file_name in os.listdir(os.path.abspath(os.path.join(os.path.dirname(__file__)))):
        if not file_name.startswith('_') and file_name.endswith('.py'):
            module = importlib.import_module('.{}'.format(file_name.replace('.py', '')), package=__package__)
            if hasattr(module, 'PostProcess'):
                modules.append(module.PostProcess)
    return modules


class PostProcess(object):
    """
    The base post-process class.
    Contains variables and methods required to run a Piper post-publishing process.
    """

    name = None  # a name for the process;
    description = None  # a description for the process;
    required = False  # whether the process is required/can be skipped by the user;
    checked = False  # whether the process is enabled by default;
    priority = 1  # the priority of this process;

    def __init__(self, dcc_api, version, parent=None):
        self.logger = get_logger('Post-process "{}"'.format(self.name))
        self.dcc_api = dcc_api
        self.version = version
        self.parent = parent
        self.piper_api = version.api

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the process.
        :return: True/False;
        """
        return True
