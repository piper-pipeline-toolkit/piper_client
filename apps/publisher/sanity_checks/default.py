# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from . import SanityCheck as SanityCheckBase


class SanityCheck(SanityCheckBase):

    name = 'Default'
    description = 'Default Piper publishing sanity check.'
    required = True
    checked = True
    priority = 100

    def __init__(self, **kwargs):
        super(SanityCheck, self).__init__(**kwargs)

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the sanity check.
        :return: True/False;
        """
        file_name = self.dcc_api.get_current_file_name()
        if not file_name:
            self.logger.error('No file name found - save before publishing.')
            return False
        return True

    def fix(self):
        """
        A method that attempts a fix should the sanity check fail (return False) on run.
        :return: True/False;
        """
        self.dcc_api.save_file()
        return True
