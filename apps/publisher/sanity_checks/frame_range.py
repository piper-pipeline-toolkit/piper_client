# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from . import SanityCheck as SanityCheckBase


class SanityCheck(SanityCheckBase):

    name = 'Frame range'
    description = 'Correct shot frame range sanity check.'
    required = False
    checked = True
    priority = 98

    def __init__(self, **kwargs):
        super(SanityCheck, self).__init__(**kwargs)
        self.start_frame = None
        self.end_frame = None

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        if self.resource.task.shot:
            return True
        return False

    def run(self):
        """
        The main function executed by the sanity check.
        :return: True/False;
        """
        shot = self.resource.task.shot
        self.start_frame = self.piper_api.get_top_configuration(self.resource, 'start_frame')

        if self.start_frame:
            self.start_frame -= shot.pre_roll
            current_start_frame = self.dcc_api.get_playback_start_frame()
            if self.start_frame != current_start_frame:
                self.logger.error('Scene start frame {0} does not match the configured start frame of {1}.'.format(
                    current_start_frame,
                    self.start_frame))
                return False

            if shot.duration:
                self.end_frame = self.start_frame + shot.duration - 1 + shot.post_roll
                current_end_frame = self.dcc_api.get_playback_end_frame()
                if self.end_frame != current_end_frame:
                    self.logger.error('Scene end frame {0} does not match the configured end frame of {1}.'.format(
                        current_end_frame,
                        self.end_frame))
                    return False

        return True

    def fix(self):
        """
        A method that attempts a fix should the sanity check fail (return False) on run.
        :return: True/False;
        """

        if self.start_frame:
            self.dcc_api.set_playback_start_frame(self.start_frame)
            self.dcc_api.set_render_start_frame(self.start_frame)
            self.logger.info('Set start frame to {}.'.format(self.start_frame))
        else:
            self.logger.warn('Could not find start frame configuration.')

        if self.end_frame:
            self.dcc_api.set_playback_end_frame(self.end_frame)
            self.dcc_api.set_render_end_frame(self.end_frame)
            self.logger.info('Set end frame to {}.'.format(self.end_frame))
        else:
            if self.start_frame:
                self.dcc_api.set_playback_end_frame(self.start_frame + 10)
                self.dcc_api.set_render_end_frame(self.start_frame + 10)
            self.logger.warn('Shot duration not set, could not set end frame.')

        return True
