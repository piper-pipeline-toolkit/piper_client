# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import argparse
import tempfile
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')))
from piper_client.config import PIPER_INIT, PIPER_SITE_PACKAGES
sys.path.append(PIPER_SITE_PACKAGES)
from piper_client.core.logger import get_logger
from piper_client.core.profiler import Profiler
from piper_client.core.utils import environment_check, run_in_terminal, piper_except_hook
from piper_client.core.api import PiperClientApi


class Launcher:
    """
    An application for configuring and launching software inside the Piper environment.
    """
    def __init__(self, profile, app, temp_file=None):
        self.logger = get_logger('Piper Client Launcher')
        self.api = PiperClientApi()
        self.profiler = Profiler()
        self.profile = profile
        self.app = app
        if not temp_file:
            _, temp_file = tempfile.mkstemp()
            temp_file += '_piper_env'
        self.temp_file = temp_file
        if sys.platform == 'win32':
            self.temp_file += '.bat'
        else:
            self.temp_file += '.sh'
        os.environ['PIPER_ENV_FILE'] = self.temp_file
        self.software = dict()
        self._setup_data()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        os.environ['PIPER_SYSTEM_ID'] = str(self.api.system.id)
        self.load_profile()

    def load_profile(self):
        """
        Load the current profile file and display its contents.
        :return: None;
        """

        if self.profile:
            if self.profile in self.profiler.profiles:
                self.logger.info('Loading the "%s" profile.' % self.profile)
                if self.app:
                    software = self.api.get_one(entity_type='Software',
                                                filters={'long_name': self.app, 'parent': self.api.system},
                                                fields=['system', 'executable', 'module_environment_variable'])
                    self._process_software(software)
                else:
                    for software in self.profiler.profiles[self.profile]['software']:
                        self._process_software(software)
                # Store the profile in an environment variable for reference outside of this class:
                os.environ['PIPER_PROFILE'] = self.profile
            else:
                self.logger.error('Could not find the "%s" profile.' % self.profile)

    def _process_software(self, software):
        if software and software.system.id == self.api.system.id:
            dcc_bin = software.executable
            if self.api.system.software_root and not dcc_bin.startswith(
                    self.api.system.software_root):
                if dcc_bin.startswith('/') or dcc_bin.startswith('\\'):
                    dcc_bin = dcc_bin[1:]
                executable = os.path.join(self.api.system.software_root, dcc_bin)
            if os.path.exists(executable):
                self.software[software.long_name] = dict()
                self.software[software.long_name]['entity'] = software
                self.software[software.long_name]['executable'] = executable

    def start_app(self, app=None):
        """
        Prepare the environment and start an application.
        :return: None;
        """
        if not app:
            app = self.app
        if app in self.software:
            self.set_environment_varaibles(dcc_name=app)
            self.logger.info(msg='Launching %s...' % app)
            # Start the application as a separate process:
            self.logger.debug('Running %s' % self.software[app]['executable'])
            if sys.platform == 'win32':
                executable = r'"{}"'.format(self.software[app]['executable'])
            else:
                executable = self.software[app]['executable'].replace(' ', '\ ')

            # Prepare start command for the current OS:
            if sys.platform == 'win32':
                command = 'call {ENV_SCRIPT} && {EXECUTABLE}'
            elif sys.platform == 'darwin':
                command = 'source {ENV_SCRIPT}; open {EXECUTABLE}'
            else:
                command = 'call {ENV_SCRIPT} && {EXECUTABLE}'
            command = command.format(ENV_SCRIPT=self.temp_file, EXECUTABLE=executable)

            run_in_terminal(command=command)
        else:
            self.logger.error('Could not find valid app: %s' % app)

    def set_environment_varaibles(self, dcc_name=None):
        """
        Set the environment variables associated with a given DCC.
        :param dcc_name: the name of the DCC (string);
        :return: None;
        """

        if not dcc_name:
            dcc_name = self.app

        # Store software details in the environment:
        os.environ['PIPER_SOFTWARE_ID'] = str(self.software[dcc_name]['entity']['id'])
        os.environ['PIPER_DCC'] = dcc_name

        profile = self.profiler.profiles[self.profile]
        profile_ids = [module.id for module in profile.modules]

        # Get the enabled modules only:
        modules = self.api.get(entity_type='Module',
                               filters={'software': self.software[dcc_name]['entity']},
                               fields=['root'])

        env_variables_set = []

        # Append the modules' path to the relevant DCC modules environment variable:
        for module in modules:
            if module.id in profile_ids:
                module_path = module.root
                if self.api.system.modules_root:
                    if module.root.startswith('/') or module.root.startswith('\\'):
                        module_root = module.root[1:]
                    else:
                        module_root = module.root
                    module_path = os.path.abspath(os.path.join(self.api.system.modules_root, module_root))
                self.logger.info('Loading module: %s' % module_path)
                self.logger.info('Loading module: %s' % module_path)
                module_environment_variable = self.software[dcc_name]['entity']['module_environment_variable']
                if not module_environment_variable:
                    continue
                for env_variable in module_environment_variable.replace(' ', '').split(','):
                    if environment_check(env_variable):
                        os.environ[env_variable] = '{0};{1}'.format(os.environ[env_variable], module_path)
                    else:
                        os.environ[env_variable] = '{};'.format(module_path)
                        if sys.platform == 'win32':
                            os.environ[env_variable] += '^&'
                    env_variables_set.append(env_variable)

        os.environ['PIPER_USER'] = self.api.user_name
        env_variables_set += [env for env in os.environ if env.startswith('PIPER')]
        self.save_environment(env_variables=env_variables_set)
        self.logger.info('Set environment based on the %s profile.' % self.profile)

    def save_environment(self, env_variables):
        """
        Store the given environment variables to a temporary file.
        :param env_variables: a list of environment variable keys;
        :return: None;
        """
        env = ''
        if sys.platform == 'win32':
            for env_variable in env_variables:
                env += 'set {0}={1}\n'.format(env_variable, os.environ[env_variable])
        else:
            for env_variable in env_variables:
                env += 'export {0}={1}\n'.format(env_variable, os.environ[env_variable])
        env_file = open(self.temp_file, 'w+')
        env_file.write(env)
        env_file.close()
        self.logger.info('Saved environment to file: %s' % self.temp_file)


def launch(profile, app, environment_only=False, temp_file=None):
    """
    Use Launcher to start an application in the context of a given profile.
    :param profile: the name of the profile (string);
    :param app: the name of the application (string);
    :param environment_only: (bool) specify if the launcher should only set the environment without running the app;
    :param temp_file: a (str) temporary file path to save any resulting environment files;
    :return: None;
    """
    launcher = Launcher(profile=profile, app=app, temp_file=temp_file)
    if environment_only:
        launcher.set_environment_varaibles()
    else:
        launcher.start_app()


if __name__ == '__main__':
    sys.excepthook = piper_except_hook
    parser = argparse.ArgumentParser(description='Piper v{} Launcher'.format(os.environ['PIPER_VERSION']))
    parser.add_argument('-p', '--profile', help='A specific Piper profile name.', required=True)
    parser.add_argument('-a', '--app', help='The name of the application to run.', required=True)
    parser.add_argument('-e', '--environment_only', dest='environment_only', action='store_true',
                        help='Setup the appropriate environment without starting the app.')
    parser.add_argument('-t', '--temp_file', help='A temporary file to store environment variables.',
                        required=False)
    args = vars(parser.parse_args())
    if args['profile'] and args['app']:
        launch(profile=args['profile'], app=args['app'], environment_only=args['environment_only'],
               temp_file=args['temp_file'])
