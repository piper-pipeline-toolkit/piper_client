# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import sys
import os
from subprocess import Popen


def launch():
    current_directory = os.path.dirname(__file__)
    if sys.platform.startswith('linux'):
        command = 'source {}'.format(os.path.join(current_directory,
                                                  'builds',
                                                  'PiperLauncher-linux-x64',
                                                  'PiperLauncher'))
    elif sys.platform == 'darwin':
        command = 'open -a {}'.format(os.path.join(current_directory,
                                                   'builds',
                                                   'PiperLauncher-darwin-x64',
                                                   'PiperLauncher.app'))
    else:
        command = os.path.join(current_directory,
                               'builds',
                               'PiperLauncher-win32-x64',
                               'PiperLauncher.exe')

    return Popen(command, shell=True, close_fds=True, env=os.environ)
