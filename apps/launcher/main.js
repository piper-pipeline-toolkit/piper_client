const { app, BrowserWindow } = require('electron')
const { ipcMain } = require('electron')
const fixPath = require('fix-path');
fixPath();
var fs  = require('fs');
var os = require('os');
var user = os.userInfo().username;
var path = require('path');
var config = {'piper_server': '', 'piper_username': '', 'piper_password': ''};
var tmp_file =  process.env.PIPER_TEMP + '/_piper_config.json';

if (fs.existsSync(tmp_file)) {
    fs.readFile(tmp_file, 'utf8', function readFileCallback(err, data) {
        if (err){
            console.log(err);
        } else {
        config = JSON.parse(data);
    }});
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win
let {PythonShell} = require('python-shell')

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ width: 800, height: 600 })

  // and load the index.html of the app.
  win.loadFile('index.html')

  // Open the DevTools.
  //win.webContents.openDevTools()

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

function open_in_browser(arg) {
    PythonShell.runString('import sys;import webbrowser;webbrowser.open(sys.argv[1]);print("Opening "+sys.argv[1])',
    { args: [arg['url']]}, function (err, results) {
        if (err) throw err;
        // results is an array consisting of messages collected during execution
        console.log('[Piper Client] > %j', results);
    });
}

ipcMain.on('server-requested', (event, arg) => {
    if (fs.existsSync(tmp_file)) {
        fs.readFile(tmp_file, 'utf8', function readFileCallback(err, data){
            if (err){
                console.log(err);
            } else {
            config = JSON.parse(data);
            return event.sender.send('server-received', {'server': config.piper_server});
        }});
    } else {
        fs.writeFile (tmp_file, JSON.stringify((config)), function(err) {
            if (err) throw err;
        });
        return event.sender.send('server-received', {'server': config.piper_server});
    }
})

ipcMain.on('credentials-requested', (event, arg) => {
    if (fs.existsSync(tmp_file)) {
        fs.readFile(tmp_file, 'utf8', function readFileCallback(err, data) {
            if (err){
                console.log(err);
            } else {
            config = JSON.parse(data);
            return event.sender.send('credentials-received',
                {'piper_username': config.piper_username,
                 'piper_password': config.piper_password});
        }});
    } else {
        return event.sender.send('credentials-received',
            {'piper_username': config.piper_username,
             'piper_password': config.piper_password});
    }
})

ipcMain.on('user-requested', (event, arg) => {
    return event.sender.send('user-received', {'user': user});
})

ipcMain.on('server-updated', (event, arg) => {
    config.piper_server = arg['server'];
    json = JSON.stringify(config);
    fs.writeFile (tmp_file, JSON.stringify((config)), function(err) {
        if (err) throw err;
    });
})

ipcMain.on('credentials-updated', (event, arg) => {
    config.piper_username = arg['credentials-username'];
    config.piper_password = arg['credentials-password'];
    json = JSON.stringify(config);
    fs.writeFile (tmp_file, JSON.stringify((config)), function(err) {
        if (err) throw err;
    });
})

ipcMain.on('user-link-clicked', (event, arg) => {
    return open_in_browser(arg);
})

ipcMain.on('logo-clicked', (event, arg) => {
    return open_in_browser(arg);
})

ipcMain.on('app-icon-clicked', (event, arg) => {
    if (fs.existsSync(arg['root'] + '/' + arg['executable'])) {
        event.sender.send('asynchronous-reply', {'message': 'Starting ' + arg['app'], 'status': 1});
        PythonShell.run(process.env.PIPER + '/apps/launcher/main.py',
                        { args: ['-p', arg['profile'], '-a', arg['app']], env: process.env,},
        function (err, results) {
            if (err) throw err;
            // results is an array consisting of messages collected during execution
            console.log('[Piper Client] > %j', results);
        });
    } else {
        return event.sender.send('asynchronous-reply', {'message': 'Could not find application: '
                                                        + arg['root'] + '/' + arg['executable'], 'status': 0});
    }
})

ipcMain.on('browse-btn-clicked', (event, arg) => {
    if (fs.existsSync(arg['root'] + '/' + arg['executable'])) {
        var command = 'import sys;import os;from subprocess import call;path = os.path.dirname(sys.argv[1]);';
        switch (process.platform) {
            case 'darwin':
                command += 'call(["open", path])';
                break;
            case 'win32':
                command += 'os.startfile(path)';
                break;
            case 'linux':
                command += 'call(["xdg-open", path])';
                break;
        }
        PythonShell.runString(command, { args: [arg['root'] + '/' + arg['executable']]}, function (err, results) {
            if (err) throw err;
            // results is an array consisting of messages collected during execution
            console.log('[Piper Client] > %j', results);
        });
    } else {
        return event.sender.send('asynchronous-reply', {'message': 'Could not find application: '
                                                        + arg['root'] + '/' + arg['executable'], 'status': 0});
    }
})