const { ipcRenderer } = require('electron')
var piper_server_url;
var user;
var stack_bar_bottom = {'dir1': 'up', 'dir2': 'right', 'spacing1': 0, 'spacing2': 0};

$( document ).ready(function() {
    ipcRenderer.send('user-requested');
    ipcRenderer.send('server-requested');
    ipcRenderer.send('credentials-requested');
    $('.selectpicker').selectpicker();
    $('#server-dialog-trigger').trigger('click');
});

function populate_selectpicker(url, id) {
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            $.each(data, function (i, item) {
                $('#' + id).append($('<option>', {
                    value: item.id,
                    text : item.long_name
                }));
                $('#' + id).selectpicker('refresh');
            });
        },
        error: function (x, status, error) {
            return connection_error(url);
        }
    })
}

function clear_selectpicker(id) {
    $('#' + id).find('[value!=""]').remove();
    $('#' + id).selectpicker('refresh');
}

function url_sanity_check(url) {
    if (!/^(f|ht)tps?:\/\//i.test(url)) {
        url = "http://" + url;
    }
    if (url.charAt(url.length - 1) == '/') {
        url = url.slice(0, -1)
    }
    return url;
}

$('#profile-select').on('change', function() {
    var value = this.value;
    var url = piper_server_url + '/Profile/' + value + '/software/';
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            $('#software').fadeOut(function() {
                $('#software').html(data.replace(/PIPER_SERVER_URL/g, piper_server_url)).fadeIn();
            });
        },
        error: function (x, status, error) {
            return connection_error(url);
        }
    }).done(function() {
        console.log('Software loaded.');
    });
});

$('#server-form').on('submit', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $('#launcher-icon').fadeOut(function() {
        $('#launcher-icon').removeClass('fa-rocket').addClass('fa-spinner').addClass('fa-spin').fadeIn();
    });

    piper_server_url = url_sanity_check($('#server-input').val());
    ipcRenderer.send('server-updated', {'server': piper_server_url});

    clear_selectpicker('profile-select');
    populate_selectpicker(piper_server_url + '/api/studio/profiles', 'profile-select');
    var url = piper_server_url + '/api/User/' + user;
    var status = 0;
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            status = 1;
            $('#username').text(user);
            $('#department').text(data.department.long_name);
            $('#user-thumbnail').attr('src', piper_server_url + data.thumbnail_url);
            $('#user-link').attr('href', piper_server_url + data.full_url);
            $('#user-link').addClass('appear-animation bounceIn appear-animation-visible');
            var notice = new PNotify({
                title: 'Connected',
                text: 'Piper server reached!',
                type: 'success',
                addclass: 'stack-bar-bottom',
                width: "70%",
                stack: stack_bar_bottom
            });
        },
        error: function (x, status, error) {
            return connection_error(url);
        }
    }).done(function() {
        $('#url-cancel').fadeIn();
        authenticate();
    });

});

function authenticate() {
    var username = $('#credentials-username-input').val();
    var password = $('#credentials-password-input').val();

    ipcRenderer.send('credentials-updated', {'credentials-username': username, 'credentials-password': password});

    var url = piper_server_url + '/api/authenticate/' + username + '/' + password;
    var status = 0;
    var message;
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            status = data.status;
            message = data.message;
        },
        error: function (x, status, error) {
            return connection_error(url);
        }
    }).done(function() {
        var type;
        if (status) {
            type = 'success';
        } else {
            type = 'error';
        }
        var notice = new PNotify({
            title: 'Authentication',
            text: message,
            type: type,
            addclass: 'stack-bar-bottom',
            width: "70%",
            stack: stack_bar_bottom
        });
        return icon_change(status);
    });
}

function icon_change(status) {
    $('#launcher-icon').fadeOut(function() {
        var icon = 'fa-times-circle';
        if (status) {
            icon = 'fa-rocket';
        }
        $('#launcher-icon').removeClass('fa-spinner').removeClass('fa-spin').addClass(icon).fadeIn();
    });
}

function connection_error(url) {
    var notice = new PNotify({
        title: 'Connection Failed',
        text: 'Could not reach ' + url,
        type: 'error',
        addclass: 'stack-bar-bottom',
        width: "70%",
        stack: stack_bar_bottom
    });
    icon_change(0);
    return false;
}

function run($source) {
    var app = $source.closest('.app')
    return ipcRenderer.send('app-icon-clicked', {'profile': $('#profile-select').find('option:selected').text(),
                                                 'app': app.data('name'),
                                                 'executable': app.data('executable'),
                                                 'root': app.data('root')
                                                 });
}

function browse($source)  {
    var app = $source.closest('.app')
    return ipcRenderer.send('browse-btn-clicked', {'profile': $('#profile-select').find('option:selected').text(),
                                                   'app': app.data('name'),
                                                   'executable': app.data('executable'),
                                                   'root': app.data('root')
                                                   });
}

$('#logo').on('click', function(event) {
    ipcRenderer.send('logo-clicked', {'url': piper_server_url});
});

$('#user-link').on('click', function(event) {
    return ipcRenderer.send('user-link-clicked', {'url': $('#user-link').attr('href')});
});

$('#software').on('click', '.browse-btn', function(event) {
    $(this).fadeOut();
    $(this).fadeIn();
    return browse($(this));
});

$('#software').on('click', '.app-icon', function(event) {
    $(this).fadeOut();
    $(this).fadeIn();
    return run($(this));
});

$('#software').on('click', '.run-btn', function(event) {
    $(this).fadeOut();
    $(this).fadeIn();
    return run($(this));
});

ipcRenderer.on('user-received', (event, data) => {
    user = data.user;
})

ipcRenderer.on('server-received', (event, data) => {
    piper_server_url = data.server;
    $('#server-input').val(piper_server_url);
})

ipcRenderer.on('credentials-received', (event, data) => {
    $('#credentials-username-input').val(data['piper_username']);
    $('#credentials-password-input').val(data['piper_password']);
})

ipcRenderer.on('asynchronous-reply', (event, result) => {
    if (result.status) {
        var notice = new PNotify({
            title: 'Success',
            text: result.message,
            type: 'success',
            addclass: 'stack-bar-bottom',
            width: "70%",
            stack: stack_bar_bottom
        });
    } else {
        var notice = new PNotify({
            title: 'Error',
            text: result.message,
            type: 'error',
            addclass: 'stack-bar-bottom',
            width: "70%",
            stack: stack_bar_bottom
        });
    }
})
