echo Starting Piper Launcher...
@echo off
set PIPER=%~dp0
set PIPER_TEMP=%TEMP%
call %PIPER%\piper_env.bat
python %PIPER%\run.py
pause