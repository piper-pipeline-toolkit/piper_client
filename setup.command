echo Setting up Piper Client, please wait...
export PIPER=$PWD
export PIPER_OS=darwin
cd $PIPER/resources/darwin
source $PIPER/resources/darwin/setup_python.command
source $PIPER/resources/darwin/setup_nodejs.command
cd $PIPER
$PIPER/resources/darwin/Python37/bin/python3.7 -m virtualenv venv_darwin -p $PIPER/resources/darwin/Python37/bin/python3.7
source $PIPER/venv_darwin/bin/activate
pip install -r requirements.txt
cd $PIPER/apps/launcher
$PIPER/resources/darwin/NodeJS/lib/node_modules/npm/bin/npm-cli.js install . --scripts-prepend-node-path && $PIPER/resources/darwin/NodeJS/lib/node_modules/npm/bin/npm-cli.js run package-mac --scripts-prepend-node-path
cd $PIPER
echo Piper Client setup completed.