# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""

import sys
import os
sys.path.append(os.environ['PIPER_INIT'])
sys.path.append(os.environ['PIPER_SITE_PACKAGES'])
import nuke
from piper_client.config.dcc_config import DCC_MENU_TEMPLATE
from piper_client.core.logger import get_logger
from piper_client.core.utils import piper_except_hook
sys.excepthook = piper_except_hook

LOGGER = get_logger('Piper Setup - Nuke')


def startup():
    """
    Run Piper start up methods - build the Piper menu, load the startup file and run Scene Builder.
    :return: None;
    """
    build_piper_menu()
    # exec(DCC_MENU_TEMPLATE['main']['Scene Builder'])
    LOGGER.info('Piper set up successful.')


def build_piper_menu():
    """
    Create the Piper menu in Nuke and start the Piper Scene Builder.
    :return: None;
    """
    menu_root = nuke.menu('Nuke')

    try:
        piper_menu = menu_root.menu('Piper')
        piper_menu.clearMenu()
    except Exception:
        piper_menu = menu_root.addMenu('Piper')

    for action in DCC_MENU_TEMPLATE['main']:
        add_action(action=action, parent_menu=piper_menu, cmd=DCC_MENU_TEMPLATE['main'][action])

    piper_menu.addSeparator()
    menu = piper_menu.addMenu('Utilities')
    for action in DCC_MENU_TEMPLATE['utilities']:
        add_action(action=action,
                   parent_menu=menu,
                   cmd=DCC_MENU_TEMPLATE['utilities'][action])

    piper_menu.addSeparator()
    for action in DCC_MENU_TEMPLATE['links']:
        add_action(action=action,
                   parent_menu=piper_menu,
                   cmd=DCC_MENU_TEMPLATE['links'][action])
    add_action(action='Piper Server', parent_menu=piper_menu,
               cmd='import webbrowser;webbrowser.open("{}")'.format(os.environ['PIPER_SERVER']))

    add_action(action='Documentation', parent_menu=piper_menu,
               cmd='import webbrowser;webbrowser.open("{}/docs/")'.format(os.environ['PIPER_SERVER']))

    LOGGER.info('Built Piper NUKE menu.')


def add_action(action, parent_menu, cmd):
    """
    Add a menu command.
    :param action: a (str) action name;
    :param parent_menu: the parent menu in the UI;
    :param cmd: a (str) action command script;
    :return: None;
    """
    return parent_menu.addCommand(action, cmd)
